// 1) import dependecies
import React, {Fragment} from 'react'
import ReactDOM from 'react-dom'

// 2) define the root

const root = document.querySelector("#root")

// 3) create component

const pageComponent = (
<Fragment><h1>Hello, World! </h1>
<button>Bowton</button> </Fragment>
)


// 4) render/display component

ReactDOM.render(pageComponent, root)